import pygame
from pygame.constants import K_ESCAPE
from main_TRON import *


class Button:
    def __init__(self, x, y, text):
        color = (255, 255, 255)
        smallfont = pygame.font.SysFont("Corbel", 35)
        self.color = (100, 100, 100)
        self.selected = False
        self.x = x
        self.y = y
        self.text = smallfont.render(text, True, color)

    def draw(self):
        if self.selected:
            self.color = (170, 170, 170)
        else:
            self.color = (100, 100, 100)
        pygame.draw.rect(screen, self.color, [self.x, self.y, 140, 40])
        screen.blit(self.text, (self.x, self.y))

    def select(self):
        self.selected = True

    def unselect(self):
        self.selected = False


# инициализация pygame
pygame.init()

# разрешение экрана
res = (720, 720)

# открывает окно заданного размера
screen = pygame.display.set_mode(res)

# сохраняет ширину экрана в переменную
width = screen.get_width()

# созраняет высоту экрана в переменную
height = screen.get_height()


while True:

    #  сохраняет координаты мыши (x,y) в переменной
    mouse = pygame.mouse.get_pos()
    btn1 = Button(width / 2, height / 2, "quit")
    btn2 = Button(width / 2, height / 2 - 100, "start")

    for ev in pygame.event.get():

        if ev.type == pygame.QUIT:
            pygame.quit()

        # проверка клика мыши
        if ev.type == pygame.MOUSEBUTTONDOWN:

            # если клик мыши произошел на кнопке выхода в нужных диапазонах) - выход
            if (
                width / 2 <= mouse[0] <= width / 2 + 140
                and height / 2 <= mouse[1] <= height / 2 + 40
            ):
                pygame.quit()
            # если клик мыши произошел на кнопке старт (в нужных диапазонах) - запуск игры
            if (
                width / 2 <= mouse[0] <= width / 2 + 140
                and height / 2 - 100 <= mouse[1] <= height / 2 - 60
            ):
                tron()

    # если курсор мыши находится над кнопкой, кнопка меняет цвет
    if (
        width / 2 <= mouse[0] <= width / 2 + 140
        and height / 2 <= mouse[1] <= height / 2 + 40
    ):
        btn1.select()
    else:
        btn1.unselect()

    if (
        width / 2 <= mouse[0] <= width / 2 + 140
        and height / 2 - 100 <= mouse[1] <= height / 2 - 60
    ):
        btn2.select()
    else:
        btn2.unselect()

    # заполнение экрана цветом
    screen.fill((60, 25, 60))
    btn1.draw()
    btn2.draw()

    # обновление экрана игры
    pygame.display.update()
