import pygame
from pygame.constants import K_ESCAPE
import sys

width = 600
height = 600
display = pygame.display.set_mode((width, height))
pygame.display.set_caption("Tron 2D")
clock = pygame.time.Clock()

background = (27, 79, 114)
white = (236, 240, 241)
yellow = (241, 196, 15)
darkYellow = (247, 220, 111)
red = (231, 76, 60)
darkRed = (241, 148, 138)
darkBlue = (40, 116, 166)

# мотоцикл отрисовывается как квадрат. w - стороная этого квадрата
w = 10


class Button:
    def __init__(self, x, y, text, screen):
        self.screen = screen
        color = (255, 255, 255)
        smallfont = pygame.font.SysFont("Corbel", 35)
        self.color = (100, 100, 100)
        self.selected = False
        self.x = x
        self.y = y
        self.text = smallfont.render(text, True, color)

    def draw(self):
        if self.selected:
            self.color = (170, 170, 170)
        else:
            self.color = (100, 100, 100)
        pygame.draw.rect(self.screen, self.color, [self.x, self.y, 140, 40])
        self.screen.blit(self.text, (self.x, self.y))

    def select(self):
        self.selected = True

    def unselect(self):
        self.selected = False

    def get_rect(self):
        return pygame.Rect(self.x, self.y, 140, 40)


class Menu:
    def __init__(self):
        self.bike_col2 = self.bike_col = (241, 196, 15)

    def run(self):
        # инициализация pygame
        pygame.init()

        # разрешение экрана
        res = (600, 600)

        # открывает окно заданного размера
        screen = pygame.display.set_mode(res)

        # сохраняет ширину экрана в переменную
        width = screen.get_width()

        # созраняет высоту экрана в переменную
        height = screen.get_height()

        while True:
            # сохраняет координаты мыши (x,y) в переменной
            # объявление кнопок в главном меню
            mouse = pygame.mouse.get_pos()
            btn1 = Button(width / 2 - 200, height / 2 + 30, "quit", screen)
            btn2 = Button(width / 2 - 200, height / 3 - 10, "start", screen)
            btn3 = Button(width / 2, height / 3 - 10, "settings", screen)
            for ev in pygame.event.get():

                if ev.type == pygame.QUIT:
                    pygame.quit()

                # проверка клика мыши
                if ev.type == pygame.MOUSEBUTTONDOWN:

                    # если клик мыши произошел на кнопке "quit" (в нужных диапазонах) - выход
                    if (
                        width / 2 - 200 <= mouse[0] <= width / 2 - 60
                        and height / 2 + 35 <= mouse[1] <= height / 2 + 75
                    ):
                        pygame.quit()
                    # если клик мыши произошел на кнопке "start" (в нужных диапазонах) - запуск игры
                    if (
                        width / 2 - 200 <= mouse[0] <= width / 2 - 60
                        and height / 3 - 10 <= mouse[1] <= height / 2 + 30
                    ):
                        tron(self.bike_col, self.bike_col2)
                    # если клик мыши произошел на кнопке "settings" (в нужных диапазонах) -
                    # открываетсся окно настроек
                    if (
                        width / 2 <= mouse[0] <= width / 2 + 140
                        and height / 3 - 10 <= mouse[1] <= height / 2 + 30
                    ):
                        self.settigs()

            # если курсор мыши находится над кнопкой, кнопка меняет цвет
            if (
                width / 2 - 200 <= mouse[0] <= width / 2 - 60
                and height / 2 + 30 <= mouse[1] <= height / 2 + 70
            ):
                btn1.select()
            else:
                btn1.unselect()
            if (
                width / 2 - 200 <= mouse[0] <= width / 2 - 60
                and height / 3 - 10 <= mouse[1] <= height / 2 + 30
            ):
                btn2.select()
            else:
                btn2.unselect()
            if (
                width / 2 <= mouse[0] <= width / 2 + 140
                and height / 3 - 10 <= mouse[1] <= height / 2 + 30
            ):
                btn3.select()
            else:
                btn3.unselect()

            # заполнение экрана цветом и  отрисовка кнопок
            screen.fill((60, 25, 60))
            btn1.draw()
            btn2.draw()
            btn3.draw()

            # обновление экрана игры
            pygame.display.update()
    # метод (функция) окна настроек
    def settigs(self):
        # инициализация pygame
        pygame.init()

        # разрешение экрана
        res = (600, 600)

        # открывает окно заданного размера
        screen = pygame.display.set_mode(res)

        # сохраняет ширину экрана в переменную
        width = screen.get_width()

        # созраняет высоту экрана в переменную
        height = screen.get_height()

        while True:
            # Сохраняет координаты мыши (x,y) в переменной.
            # Объявлениие кнопок. 
            mouse = pygame.mouse.get_pos()
            btn1 = Button(width / 2 - 200, height / 2 + 60, "blue", screen)
            btn2 = Button(width / 2 - 200, height / 2, "green", screen)
            btn3 = Button(width / 2 - 200, height / 2 - 60, "red", screen)
            btn4 = Button(width / 2, height / 2 + 60, "blue", screen)
            btn5 = Button(width / 2, height / 2, "green", screen)
            btn6 = Button(width / 2, height / 2 - 60, "red", screen)
            btn7 = Button(width / 2, height - 50, "start", screen)
            for ev in pygame.event.get():

                if ev.type == pygame.QUIT:
                    pygame.quit()

                # проверка клика мыши
                if ev.type == pygame.MOUSEBUTTONDOWN:

                    # если клик мыши произошел на одной из кнопок (в нужных диапазонах) 
                    # - изменение цвета мотоцикла
                    if btn1.get_rect().collidepoint(mouse):
                        self.bike_col = (0, 0, 255)
                    if btn2.get_rect().collidepoint(mouse):
                        self.bike_col = (0, 255, 0)
                    if btn3.get_rect().collidepoint(mouse):
                        self.bike_col = (255, 0, 0)
                    if btn4.get_rect().collidepoint(mouse):
                        self.bike_col2 = (0, 0, 255)
                    if btn5.get_rect().collidepoint(mouse):
                        self.bike_col2 = (0, 255, 0)    
                    if btn6.get_rect().collidepoint(mouse):
                        self.bike_col2 = (255, 0, 0)
                    if btn7.get_rect().collidepoint(mouse):
                        tron(self.bike_col1, self.bike_col2)

            # если курсор мыши находится над кнопкой, кнопка меняет цвет
            if btn1.get_rect().collidepoint(mouse):
                btn1.select()
            else:
                btn1.unselect()
            if btn2.get_rect().collidepoint(mouse):
                btn2.select()
            else:
                btn2.unselect()
            if btn3.get_rect().collidepoint(mouse):
                btn3.select()
            else:
                btn3.unselect()
            if btn4.get_rect().collidepoint(mouse):
                btn4.select()
            else:
                btn4.unselect()
            if btn5.get_rect().collidepoint(mouse):
                btn5.select()
            else:
                btn5.unselect()
            if btn6.get_rect().collidepoint(mouse):
                btn6.select()
            else:
                btn6.unselect()
            if btn7.get_rect().collidepoint(mouse):
                btn7.select()
            else:
                btn7.unselect()

            # заполнение экрана цветом
            screen.fill((60, 25, 60))
            btn1.draw()
            btn2.draw()
            btn3.draw()
            btn4.draw()
            btn5.draw()
            btn6.draw()
            btn7.draw()
            # обновление экрана игры
            pygame.display.update()


class tronBike:
    def __init__(self, number, color, darkColor, side):
        self.w = w
        self.h = w
        self.x = abs(side - 100)
        self.y = height / 2 - self.h
        self.speed = 10
        self.color = color
        self.darkColor = darkColor
        self.history = [[self.x, self.y]]
        self.number = number
        self.length = 1

    # Перебираем все значения от 0 до текущей длинны. lenght - наша длина / длина массива history.
    # В нашем местополжении рисуется клетка определенного цвета.
    # В предыдущем местоположнии рисуется клетка того же цвета, но темнее.
    def draw(self):
        for i in range(len(self.history)):
            if i == self.length - 1:
                pygame.draw.rect(
                    display,
                    self.darkColor,
                    (self.history[i][0], self.history[i][1], self.w, self.h),
                )
            else:
                pygame.draw.rect(
                    display,
                    self.color,
                    (self.history[i][0], self.history[i][1], self.w, self.h),
                )

    # Задается логика изменения координаты (направление * скорость).
    # Запись изменения координаты в историю => увелечение длины на один
    # Условия выхода за границы. Если мы достигли крайней точки по оси oХ или oУ,
    # тогда квадритик рисуется на противоположной стороне эрана
    def move(self, xdir, ydir):
        self.x += xdir * self.speed
        self.y += ydir * self.speed
        self.history.append([self.x, self.y])
        self.length += 1
        if self.x < 0:
            self.x = width
        if self.x > width:
            self.x = 0
        if self.y > height:
            self.y = 0
        if self.y < 0:
            self.y = height

    # Проверются лобовые столкновения, столкновения с соперником, столкновения со следом.
    # Лобовое столкновение: абсолютное значение разности актуальных соотв.
    # координат разных мотоциклов сравнивается с их размерами.
    # Если значние разности < значения размера => произошло лобовое столкновение
    def checkIfHit(self, opponent):
        if (
            abs(
                opponent.history[opponent.length - 1][0]
                - self.history[self.length - 1][0]
            )
            < self.w
            and abs(
                opponent.history[opponent.length - 1][1]
                - self.history[self.length - 1][1]
            )
            < self.h
        ):
            gameOver(0)

        # Столкновение со следом соперником: абсолютное значение разности предыдущих
        # координат разных мотоциклов сравнивается с их размерами.
        # Если значние разности < значения размера => произошло столкновение со следом противника
        for i in range(opponent.length):
            if (
                abs(opponent.history[i][0] - self.history[self.length - 1][0]) < self.w
                and abs(opponent.history[i][1] - self.history[self.length - 1][1])
                < self.h
            ):
                gameOver(self.number)

        # Столкновение с собственным следом : абсолютное значение разности
        # собственной координаты со всеми предыдущими сравнивается с собственным размером
        # Если значние разности < значения размера => произошло столкновение с собственным следом
        for i in range(len(self.history) - 1):
            if (
                abs(self.history[i][0] - self.x) < self.w
                and abs(self.history[i][1] - self.y) < self.h
                and self.length > 2
            ):
                gameOver(self.number)
    # ф-я ускорения
    def speedUp(self):
        self.speed *= 1.5
    # ф-я замедления
    def speedDown(self):
        self.speed /= 1.5


# Ф-я в которой прописаны возможные условия выхода. Задана кнопка перезапуска игры
def gameOver(number):
    font = pygame.font.SysFont("arial", 65)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                close()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    close()
                if event.key == pygame.K_r:
                    tron (red, yellow)
        if number == 0:
            text = font.render("Both the Players Collided!", True, white)
        else:
            text = font.render("Player %d Lost the Tron!" % (number), True, white)

        display.blit(text, (50, height / 2))

        pygame.display.update()
        clock.tick(60)


# Ф-я отрисовки сетки поля на заднем плане игры. Позже удалиться, будет вставлена картинка.
# Вычисляется сторона квадрата, чтобы делать отступ на это расстояние и рисовать линию
def drawGrid():
    squares = 50
    for i in range(int(width / squares)):
        pygame.draw.line(
            display, darkBlue, (i * squares, 0), (i * squares, height)
        )  # Вертикальные линии
        pygame.draw.line(
            display, darkBlue, (0, i * squares), (width, i * squares)
        )  # Горизонтальные линии


# Ф-я закрытия: закрывается pygame, затем окно
def close():
    pygame.quit()
    sys.exit()


# Основаная ф-я
def tron(color1, color2):
    loop = True
    dark_color1 = (
        (color1[0] + 100) % 255,
        (color1[1] + 100) % 255,
        (color1[2] + 100) % 255,
    )
    dark_color2 = (
        (color2[0] + 100) % 255,
        (color2[1] + 100) % 255,
        (color2[2] + 100) % 255,
    )
    menu = Menu()
    bike1 = tronBike(1, color1, dark_color1, 0)
    bike2 = tronBike(1, color2, dark_color2, width)

    # Начальные направления игроков
    x1 = 1
    y1 = 0
    x2 = -1
    y2 = 0

    # Обработка событий закрытия игры, нажатия клавишь управления
    # При нажатии на клавишу управления игроком происходит проверка направления,
    # если мы до нажатия не двигались в нужном направлении,
    # проиходит смена направления.

    while loop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                close()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    menu.run()
                if event.key == pygame.K_RCTRL:
                    bike2.speedUp()
                if event.key == pygame.K_LCTRL:
                    bike1.speedUp()

                if event.key == pygame.K_UP:
                    if not (x2 == 0 and y2 == 1):
                        x2 = 0
                        y2 = -1
                if event.key == pygame.K_DOWN:
                    if not (x2 == 0 and y2 == -1):
                        x2 = 0
                        y2 = 1
                if event.key == pygame.K_LEFT:
                    if not (x2 == 1 and y2 == 0):
                        x2 = -1
                        y2 = 0
                if event.key == pygame.K_RIGHT:
                    if not (x2 == -1 and y2 == 0):
                        x2 = 1
                        y2 = 0
                if event.key == pygame.K_w:
                    if not (x1 == 0 and y1 == 1):
                        x1 = 0
                        y1 = -1
                if event.key == pygame.K_s:
                    if not (x1 == 0 and y1 == -1):
                        x1 = 0
                        y1 = 1
                if event.key == pygame.K_a:
                    if not (x1 == 1 and y1 == 0):
                        x1 = -1
                        y1 = 0
                if event.key == pygame.K_d:
                    if not (x1 == -1 and y1 == 0):
                        x1 = 1
                        y1 = 0
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RCTRL:
                    bike2.speedDown()
                if event.key == pygame.K_LCTRL:
                    bike1.speedDown()

        display.fill(background)
        drawGrid()
        bike1.draw()
        bike2.draw()

        bike1.move(x1, y1)
        bike2.move(x2, y2)

        bike1.checkIfHit(bike2)
        bike2.checkIfHit(bike1)

        # Обновление экрана с заданной частотой
        pygame.display.update()
        clock.tick(10)


menu = Menu()
menu.run()
